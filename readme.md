# Rocket League Trading

- [Rocket League Trading](#rocket-league-trading)
  - [Features](#features)
    - [Rocket League Garage](#rocket-league-garage)
    - [RL Insider](#rl-insider)
  - [Permissions](#permissions)
  - [Download and Installation](#download-and-installation)
    - [Firefox](#firefox)
    - [Chrome](#chrome)
  - [FAQ](#faq)
    - [Why isn't this extension in the Firefox Add-ons store?](#why-isnt-this-extension-in-the-firefox-add-ons-store)
    - [Why isn't this extension in the Chrome extension store?](#why-isnt-this-extension-in-the-chrome-extension-store)
    - [Can you speed up the loading time of RLG?](#can-you-speed-up-the-loading-time-of-rlg)
  - [Feedback](#feedback)
  - [Donate](#donate)

## Features

### Rocket League Garage

- Dark theme
- Highlight profitable trades 
- Show item prices
- Find cheapest and most expensive trade for an item

![Preview](preview.png)

### RL Insider

- Show exact prices
- Show average prices

## Permissions
- Storage
  - To save your settings

## Download and Installation

### Firefox

1. [Download the extension](rlt.xpi)
2. Open [about:addons](about:addons)
3. Click the gear with a downward facing arrow at the top
4. Select "Install Add-on From File..."
5. Select "rlt.xpi"
6. Click "Add"
7. The add-on is now installed. Select the add-on's symbol in the add-on bar to load default settings.

### Chrome

1. [Download the extension](rlt.crx)
2. Open [chrome://extensions/](chrome://extensions/)
3. Enable Developer mode (Top right).
4. Drag and drop rlt.crx into Chrome
5. Click "Add extension"
6. The extension is now installed. Select the extension's symbol in the extension bar to load default settings.

Note: Enabling developer mode makes your browser more vulnerable to malicious files. Be careful what you download. Chrome doesn't offer auto-updates for self-hosted extensions and this extension doesn't currently notify you about updates.

## FAQ

### Why isn't this extension in the Firefox Add-ons store?
> I like having all files in one place.

### Why isn't this extension in the Chrome extension store?
> Google requires a credit card to upload extensions to their store.

### Can you speed up the loading time of RLG?
> No.

## Feedback

For feature requests and bug reports, please create an issue in the repository on GitLab or contact me on [Steam](https://steamcommunity.com/profiles/76561198026315753/).

## Donate

BTC: 1EJ5K9hpUcTkZpJSMWJnN4ZHqPyED83vVE

Steam: [Trade offer](https://steamcommunity.com/tradeoffer/new/?partner=66050025&token=mxkrZqy5)