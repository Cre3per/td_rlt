class RLGPriceLookup
{
  /**
   * Creates a url based on local item information
   * @param {number} page
   * @return {string} URL
   */
  _craftURL(page)
  {
    let url = 'https://rocket-league.com/trading?';

    url += ('filterItem=' + this._item.toString() + '&');
    url += ('filterCertification=' + this._certification.toString() + '&');
    url += ('filterPaint=' + this._paint.toString() + '&');
    url += ('filterPlatform=' + this._platform.toString() + '&');
    url += ('filterSearchType=' + this._searchType.toString() + '&');

    if (page > 1)
    {
      // p=1 causes wrong results
      url += ('p=' + page.toString());
    }

    return url;
  }

  /**
   * Searches for the local item in a trade container
   * @param {HTMLDivElement} displayItemsContainer
   * @return {number[]} The item's indexes in the container
   */
  _findItems(displayItemsContainer)
  {
    let items = [];

    let i = 0;

    for (let item of displayItemsContainer.children)
    {
      ++i;

      let itemInfo = RLGPriceLookup.parseURL(item.href);

      if ((this._item !== 0) && (itemInfo.item !== this._item))
      {
        continue;
      }

      if ((this._certification !== '0') && ((itemInfo.certification !== this._certification) && (itemInfo.certification !== '0')))
      {
        continue;
      }

      if ((this._paint !== '0') && (itemInfo.paint !== this._paint) && (itemInfo.paint !== '0'))
      {
        continue;
      }

      items.push(i - 1);
    }

    return items;
  }

  /**
   * Crawls items from a single page
   * @param {Document} doc The page's document
   * @param {boolean} ignoreUnAmounted Ignore items with no amount
   * @return {{ trade:string, offer:number, hrefOwner:string }[]} Offered keys
   */
  _crawlSinglePage(doc, ignoreUnAmounted)
  {
    let CURRENCY = {};

    CURRENCY[496] = 1.0; // Key
    CURRENCY[1298] = 1.2; // Egg

    let trades = doc.querySelectorAll('.rlg-trade-display-items');
    let offers = [];

    for (let trade of trades)
    {
      let yourItems = trade.querySelector('#rlg-youritems');
      let theirItems = trade.querySelector('#rlg-theiritems');

      let yourItemCount = yourItems.children.length; // Left
      let theirItemCount = theirItems.children.length; // Right

      if (yourItemCount === theirItemCount)
      {
        let itemInventory = ((this._searchType === 1) ? yourItems : theirItems);
        let currencyInventory = ((this._searchType === 1) ? theirItems : yourItems);

        let itemIndexes = this._findItems(itemInventory);

        if (itemIndexes.length === 0)
        {
          console.warn(`Item not found in trade, although we searched`);
          console.warn(itemInventory);
        }
        else
        {
          for (let itemIndex of itemIndexes)
          {
            let offerItem = currencyInventory.children[itemIndex];
            let offerInfo = RLGPriceLookup.parseURL(offerItem.href);

            if (Object.keys(CURRENCY).includes(offerInfo.item.toString()))
            {
              let currency = CURRENCY[offerInfo.item];
              let displayItemAmount = offerItem.querySelector('.rlg-trade-display-item__amount');

              let currencyAmount = (displayItemAmount ? parseInt(displayItemAmount.innerText) : 1);

              if (ignoreUnAmounted && !displayItemAmount)
              {
                if (trade.parentElement.querySelector('.rlg-trade-note-container'))
                {
                  // Using the message instead of the trade for their numbers
                  continue;
                }
              }

              let hrefOwner = trade.parentElement.querySelector('.rlg-trade-player-link').href;
              let tradeURL = trade.parentElement.querySelector('.rlg-trade-display-header > a').href;

              let offer = (currencyAmount * currency);

              displayItemAmount = itemInventory.children[itemIndex].querySelector('.rlg-trade-display-item__amount');
              let itemAmount = (displayItemAmount ? parseInt(displayItemAmount.innerText) : 1);

              offer /= itemAmount;

              offers.push({ trade: tradeURL, offer: offer, hrefOwner: hrefOwner });
            }
          }
        }
      }
    }

    return offers;
  }

  /**
   * @callback RLGPriceLookup~runCallback
   * @param {?Object} err An error object
   * @param {number} progress 0.0 - 1.0
   * @param {?{ trade:string, offer:number }[]} offers
   */
  /**
   * Runs the search
   * @param {number} maxPages
   * @param {boolean} ignoreUnAmounted Ignore items with no set amount
   * @param {RLGPriceLookup~runCallback} callback
   */
  run(maxPages, ignoreUnAmounted, callback)
  {
    // If @maxPages gets too big, we'll need to stop sending all requests at
    // once.
    let startPage = 1;
    let offers = [];
    let promises = [];

    let finishedPages = 0;

    for (let i = startPage; i < (startPage + maxPages); ++i)
    {
      promises.push(new Promise((resolve, reject) =>
      {
        let xhr = new XMLHttpRequest();

        xhr.responseType = 'document';

        xhr.onload = () =>
        {
          ++finishedPages;

          if (xhr.responseXML)
          {
            let newOffers = this._crawlSinglePage(xhr.responseXML, ignoreUnAmounted);

            // Remove duplicate trades
            for (let i = 0; i < newOffers.length; ++i)
            {
              if (offers.find((el) => { return (el.hrefOwner === newOffers[i].hrefOwner); }))
              {
                newOffers.splice(i, 1);
                --i;
                break;
              }
            }

            offers.push(...newOffers);
          }
          else
          {
            console.warn(`Request for page ${i} doesn't have a document. Ignoring.`);
          }

          if (finishedPages !== maxPages)
          {
            callback(null, finishedPages / maxPages, offers);
          }

          resolve();
        };

        xhr.onerror = () =>
        {
          ++finishedPages;
          // TODO:
          console.warn(`Request for page ${i} failed. Ignoring.`);

          if (finishedPages !== maxPages)
          {
            callback(null, finishedPages / maxPages, offers);
          }

          resolve();
        };

        xhr.open('GET', this._craftURL(i));
        xhr.send();
      }));
    }

    Promise.all(promises).then((values) =>
    {
      callback(null, 1.0, offers);
    });
  }

  /**
   * @param {number} item
   * @param {string} certification
   * @param {string} paint
   * @param {number} searchType
   * @return {boolean}
   */
  setItem(item, certification, paint, platform, searchType)
  {
    this._item = item;
    this._certification = certification;
    this._paint = paint;
    this._platform = platform;
    this._searchType = searchType;

    if (this._searchType === 0)
    {
      return false;
    }

    return true;
  }

  /**
   * @param {string} url
   * @return {boolean}
   */
  setItemFromURL(url)
  {
    let parsed = RLGPriceLookup.parseURL(url);

    return this.setItem(parsed.item, parsed.certification, parsed.paint, parsed.platform, parsed.searchType);
  }

  /**
   * 
   */
  constructor()
  {
    /**
     * @type {number}
     */
    this._item = 0;

    /**
     * @type {string}
     */
    this._certification = 'N';

    /**
     * @type {string}
     */
    this._paint = 'N';

    /**
     * @type {number}
     */
    this._platform = 0;

    /**
     * @type {number}
     */
    this._searchType = 0;
  }

  /**
   * Extracts search information from a URL
   * @param {string} url
   * @return {{ item:number, certification:string, paint:string, platform:number, searchType:number, page:number }}
   */
  static parseURL(url)
  {
    let result = {};

    result.item = parseInt(/filterItem\=(\d+)/g.exec(url)[1]);
    result.certification = /filterCertification\=(\d+|N)/g.exec(url)[1];
    result.paint = /filterPaint\=(\d+|N)/g.exec(url)[1];
    result.platform = parseInt(/filterPlatform\=(\d+)/g.exec(url)[1]);

    let typeExpr = /filterSearchType\=(\d+)/g.exec(url);

    if (typeExpr && typeExpr[1])
    {
      result.searchType = parseInt(typeExpr[1]);
    }
    else
    {
      // Item <a>'s don't have a filterSearchType
      result.searchType = 0;
    }

    let pageExpr = /p\=(\d+)/g.exec(url);

    if (pageExpr && pageExpr[1])
    {
      result.page = parseInt(pageExpr[1]);
    }
    else
    {
      result.page = 1;
    }

    return result;
  }
}