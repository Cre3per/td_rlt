class ItemStack
{
  /**
   * @return {Item}
   */
  getItem()
  {
    return this._item;
  }

  /**
   * @return {number}
   */
  getSize()
  {
    return this._size;
  }

  /**
   * @param {Item} item
   */
  setItem(item)
  {
    this._item = item;
  }

  /**
   * @param {number} size
   */
  setSize(size)
  {
    this._size = size;
  }

  /**
   * @return {number} Stack size multiplied by item price
   */
  getPrice()
  {
    return (this._item.getPrice() * this._size);
  }

  /**
   * @param {Item} item The item the stack consists of
   * @param {number} size Stack size
   */
  constructor(item = null, size = 1)
  {
    /**
     * The item the stack consists of
     * @type {Item}
     */
    this._item = item;

    /**
     * Stack size
     * @type {number}
     */
    this._size = size;
  }
}