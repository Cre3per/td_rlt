/**
 * Property separation is based on
 * {@link https://rocketleague.wikia.com/wiki/Zephyr_Crate}.
 * I am purposefully not using enums to increase forward compatibility.
 */
class Item
{
  /**
   * @return {string} Icon URL
   */
  getIcon()
  {
    return this._icon;
  }

  /**
   * @return {string} Name
   */
  getName()
  {
    return this._name;
  }

  /**
   * @return {string} Type
   */
  getType()
  {
    return this._type;
  }

  /**
   * @return {string} Certification
   */
  getCertification()
  {
    return this._certification;
  }

  /**
   * @return {string} Vehicle
   */
  getVehicle()
  {
    return this._vehicle;
  }

  /**
   * @return {string} Rarity
   */
  getRarity()
  {
    return this._rarity;
  }

  /**
   * @return {string} Paint
   */
  getPaint()
  {
    return this._paint;
  }

  /**
   * @return {number} Price
   */
  getPrice()
  {
    return this._price;
  }

  /**
   * @return {boolean} Whether or not the item is new
   */
  isNew()
  {
    return this._isNew;
  }

  /**
   * @param {string} sourceName Source identifier
   * @return {?string} Source URL
   */
  getSource(sourceName)
  {
    return this._sources[sourceName];
  }
  
  /**
   * @param {string} icon Icon URL
   */
  setIcon(icon)
  {
    this._icon = icon;
  }

  /**
   * @param {string} name Name
   */
  setName(name)
  {
    this._name = name;
  }

  /**
   * @param {string} type Type
   */
  setType(type)
  {
    this._type = type;
  }

  /**
   * @param {string} certification Certification
   */
  setCertification(certification)
  {
    this._certification = certification;
  }

  /**
   * @param {string} vehicle Vehicle
   */
  setVehicle(vehicle)
  {
    this._vehicle = vehicle;
  }

  /**
   * @param {string} rarity Rarity
   */
  setRarity(rarity)
  {
    this._rarity = rarity;
  }

  /**
   * @param {string} paint Paint
   */
  setPaint(paint)
  {
    this._paint = paint;
  }

  /**
   * @param {number} price Price
   */
  setPrice(price)
  {
    this._price = price;
  }

  /**
   * @param {boolean} isNew Whether or not the item is true
   */
  setIsNew(isNew)
  {
    this._isNew = isNew;
  }
  
  /**
   * @param {string} sourceName Source identifier
   * @param {string} url Source URL
   */
  setSource(sourceName, url)
  {
    this._sources[sourceName] = url;
  }

  /**
   * @param {Item} that
   * @param {boolean} checkCertification Compare certification
   * @return {boolean} True if this.(name|type|[certification]|vehicle|rarity|paint) is equal to
   *                   {@link that}
   */
  isSame(that, checkCertification = true)
  {
    if (this._name !== that._name)
    {
      return false;
    }

    if (this._type !== that._type)
    {
      return false;
    }

    if (checkCertification && (this._certification !== that._certification))
    {
      return false;
    }

    if (this._vehicle !== that._vehicle)
    {
      return false;
    }

    if (this._rarity !== that._rarity)
    {
      return false;
    }

    if (this._paint !== that._paint)
    {
      return false;
    }

    return true;
  }


  /**
   * 
   */
  constructor(data)
  {
    /**
     * Icon url
     * @type {string}
     */
    this._icon = (data && ('icon' in data)) ? data.icon : null;

    /**
     * Name
     * @type {string}
     */
    this._name = (data && ('name' in data)) ? data.name : null;

    /**
     * Type
     * {@link http://rocketleague.wikia.com/wiki/Category:Items}
     * @type {string}
     */
    this._type = (data && ('type' in data)) ? data.type : null;;

    /**
     * Certification
     * {@link http://rocketleague.wikia.com/wiki/Certified_Items}
     * @type {string}
     */
    this._certification = (data && ('certification' in data)) ? data.certification : null;;

    /**
     * Vehicle
     * {@link http://rocketleague.wikia.com/wiki/Category:Bodies}
     * @type {string}
     */
    this._vehicle = (data && ('vehicle' in data)) ? data.vehicle : null;

    /**
     * Rarity
     * {@link http://rocketleague.wikia.com/wiki/Category:Collectables}
     * @type {string}
     */
    this._rarity = (data && ('rarity' in data)) ? data.rarity : null;;

    /**
     * Paint
     * {@link http://rocketleague.wikia.com/wiki/Painted_item}
     * @type {string}
     */
    this._paint = (data && ('paint' in data)) ? data.paint : null;

    /**
     * Price in keys
     * @type {number}
     */
    this._price = (data && ('price' in data)) ? data.price : null;

    /**
     * Whether or not the item is new.
     * @type {boolean}
     */
    this._isNew = (data && ('isNew' in data)) ? data.isNew : false;

    /**
     * Information provider URLs. Mapped by provider name.
     * @type {Object}
     */
    this._sources = { };
  }
}