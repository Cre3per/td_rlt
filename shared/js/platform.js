class Platform
{
  static get PC()
  {
    return 0;
  }

  static get PS()
  {
    return 1;
  }

  static get XBOX()
  {
    return 2;
  }

  static get SWITCH()
  {
    return 3;
  }

  /**
   * @param {number} id Platform id
   * @return {string} Platform object key
   */
  static name(id)
  {
    switch (id)
    {
      case this.PC:
        return 'pc';
      case this.PS:
        return 'ps';
      case this.XBOX:
        return 'xbox';
      case this.SWITCH:
        return 'ns';
    }

    console.warn(`Unknown platform id ${id}`);
    return undefined;
  }
}