class PriceProvider
{
  /**
   * @callback PriceProvider~loadAllPricesCallback
   * @param {?Object} err An error object
   */
  /**
   * Loads all prices.
   * Upon completion, prices are stored locally
   * @param {PriceProvider~loadAllPricesCallback} callback
   */
  loadAllPrices(callback)
  {
    console.error(`${this.loadAllPrices.name} call on super class`);
  }

  /**
   * @return {{pc: Item[], ps: Item[], xbox: Item[]}} All items
   */
  getItems()
  {
    return this._items;
  }

  /**
   * 
   */
  constructor()
  {
    /**
     * Items grouped by platform. Ids are defined by @platform.js
     * @type {{pc: Item[], ps: Item[], xbox: Item[]}}
     */
    this._items = { };
  }
}