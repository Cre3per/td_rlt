class PriceProviderInsider extends PriceProvider
{
  /**
   * Extracts items from an item row
   * @param {string} insiderPlatform Platform identifier of rl.insider.gg
   * @param {HTMLTableRowElement} itemRow The item row
   */
  _extractItems(insiderPlatform, itemRow)
  {
    const PAINTS = {
      'priceDefault': null,
      'priceWhite': 'Titanium White',
      'priceSkyBlue': 'Sky Blue',
      'priceBurntSienna': 'Burnt Sienna',
      'priceForestGreen': 'Forest Green',
    };

    const PAINT_URLS = {
      'priceWhite': 'white',
      'priceSkyBlue': 'sblue',
      'priceBurntSienna': 'sienna',
      'priceForestGreen': 'fgreen',
    };

    const TYPES = {
      'crate': 'Crate',
      'decal': 'Decal',
      'wheels': 'Wheel',
      'goalExplosion': 'Goal Explosion',
      'boost': 'Rocket Boost',
      'topper': 'Topper',
      'trail': 'Trail',
      'banner': 'Player Banner',
      'antenna': 'Antenna',
      'car': 'Body',
      'paintFinish': 'Paint Finish',
    };

    const RARITIES = {
      'common': 'Common',
      'uncommon': 'Uncommon',
      'rare': 'Rare',
      'veryRare': 'Very Rare',
      'import': 'Import',
      'exotic': 'Exotic',
      'blackMarket': 'Black Market',
      'limited': 'Limited',
    };

    const NAMES = {
      'Players Choice Crate': 'Player\'s Choice Crate',
      'Dragonlord': 'Dragon Lord',
      'Neothermal': 'Neo-Thermal',
      '20xx': '20XX',
      'Ratrods': 'Rat Rod',
      'WWE Monday Night Raw': 'WWE Monday Night RAW',
    };

    let items = [];

    for (let priceRange of itemRow.querySelectorAll('.priceRange'))
    {
      let item = new Item();

      let fullName = itemRow.getAttribute('data-itemfullname');
      let itemName = fullName;

      if (fullName.includes(' - '))
      {
        // Octane - Royal Tyrant
        let separatorIndex = fullName.indexOf(' - ');

        itemName = fullName.substring(separatorIndex + 3, fullName.length);
        item.setVehicle(fullName.substring(0, separatorIndex));
      }

      itemName = itemName.replace(' (Trail)', '').replace(' (Antenna)', '').replace(' (Topper)', '').replace(' (Boost)', '').replace(' (Banner)', '').replace(' (Goal Explosion)', '').replace(' (Wheels)', '');
      itemName = itemName.replace('(smooth)', '(Smooth)');

      if (itemName in NAMES)
      {
        item.setName(NAMES[itemName]);
      }
      else
      {
        item.setName(itemName);
      }

      let priceHistory = JSON.parse(priceRange.getAttribute('data-history'));

      if (priceHistory.length > 0)
      {
        item.setPrice(priceHistory[0]);
      }

      if (priceRange.classList.item(0) in PAINTS)
      {
        item.setPaint(PAINTS[priceRange.classList.item(0)]);
      }
      else
      {
        item.setPaint(priceRange.classList.item(0).substr(5));
      }

      if (itemRow.getAttribute('data-itemtype') in TYPES)
      {
        item.setType(TYPES[itemRow.getAttribute('data-itemtype')]);
      }
      else
      {
        item.setType(itemRow.getAttribute('data-itemtype'));
      }

      if (itemRow.getAttribute('data-itemrarity') in RARITIES)
      {
        item.setRarity(RARITIES[itemRow.getAttribute('data-itemrarity')]);
      }
      else
      {
        item.setRarity(itemRow.getAttribute('data-itemrarity'));
      }

      let sourceURL = `https://rl.insider.gg/${insiderPlatform}/${itemRow.getAttribute('data-itemurl')}`;

      if (priceRange.classList.item(0) !== 'priceDefault')
      {
        let paint = priceRange.classList.item(0);

        if (paint in PAINT_URLS)
        {
          paint = PAINT_URLS[paint];
        }
        else
        {
          paint = paint.substr(5).toLowerCase();
        }

        sourceURL += ('/' + paint);
      }

      item.setIsNew(itemRow.getAttribute('data-isnew') === '1');

      item.setSource('insider.gg', sourceURL);

      items.push(item);
    }

    return items;
  }

  /**
   * @param {Platform} platform Platform identifier
   * @param {string} insiderPlatform Platform identifier of rl.insider.gg
   * @param {Document} dom
   */
  _extractPrices(platform, insiderPlatform, dom)
  {
    this._items[Platform.name(platform)] = [];

    for (let itemRow of dom.querySelectorAll('.itemRow'))
    {
      if (itemRow.classList.contains('emptyCell'))
      {
        continue;
      }

      this._items[Platform.name(platform)].push(...this._extractItems(insiderPlatform, itemRow));
    }

    this._items[Platform.name(platform)].push(new Item({ name: 'Key', price: 1.0 }));
  }

  /**
   * @override
   */
  loadAllPrices(callback)
  {
    let platforms = {
      'pc': Platform.PC,
      'ps4': Platform.PS,
      'xbox': Platform.XBOX,
    };

    let promises = [];

    for (let platform of Object.keys(platforms))
    {
      promises.push(new Promise((resolve, reject) =>
      {
        let xhr = new XMLHttpRequest();

        xhr.responseType = 'document';

        xhr.onload = (ev) =>
        {
          if (xhr.responseXML)
          {
            this._extractPrices(platforms[platform], platform, xhr.responseXML);
            resolve(null);
          }
          else
          {
            resolve({ msg: 'No response XML', xhr: xhr, ev: ev });
          }
        };

        xhr.onerror = (ev) =>
        {
          resolve({ xhr: xhr, ev: ev });
        };

        xhr.open('GET', `https://rl.insider.gg/${platform}`);
        xhr.send();
      }));
    }

    Promise.all(promises).then((values) =>
    {
      for (let value of values)
      {
        if (value)
        {
          callback(value);
          return;
        }
      }

      callback(null);
    });
  }
}