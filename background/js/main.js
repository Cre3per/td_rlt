class Background
{
  /**
   * 
   */
  constructor()
  {
    chrome.runtime.onMessage.addListener((message, sender, sendResponse) =>
    {
      if (message === 'getPrices')
      {
        sendResponse(this._priceProvider.getItems());

        return true;
      }
      else
      {
        return false;
      }
    });

    /**
     * Price provider
     */
    this._priceProvider = new PriceProviderInsider();

    this._priceProvider.loadAllPrices((err) =>
    {
      if (err)
      {
        console.error(err);
      }
      else
      {
        console.log('Prices loaded');
        console.log(this._priceProvider._items);


        // let lookup = new RLGPriceLookup();

        // if (lookup.setItemFromURL('https://rocket-league.com/trading?filterItem=52&filterCertification=N&filterPaint=5&filterPlatform=1&filterSearchType=1'))
        // {
        //   lookup.run(1, false, (err, progress, offers) =>
        //   {
        //     if (err)
        //     {
        //       console.error(err);
        //     }
        //     else
        //     {
        //       if (progress === 1.0)
        //       {
        //         console.log(offers);
        //       }
        //       else
        //       {
        //         console.log(progress);
        //       }
        //     }
        //   });
        // }
        // else
        // {
        //   console.error('Invalid item url!');
        // }
      }
    });
  }
}

new Background();