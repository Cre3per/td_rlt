class Main
{
  /**
   * @param {HTMLTableCellElement} td The cell
   * @param {boolean} decimal Show decimal price
   * @param {boolean} average Show average price
   * @return {string} New content
   */
  _getNewCellText(td, decimal, average)
  {
    let history = JSON.parse(td.getAttribute('data-history'));

    if (history.length > 0)
    {
      let newText = '';

      if (decimal)
      {
        let currentPrice = history[0];

        newText = '';
        newText += (Math.round(currentPrice * 100.0) / 100.0).toString();
      }
      else
      {
        newText = td.innerText;
      }

      if (average)
      {
        let averagePrice = 0.0;

        for (let entry of history)
        {
          averagePrice += entry;
        }
  
        averagePrice /= history.length;

        newText += ('\n~' + (Math.round(averagePrice * 100.0) / 100.0).toString());
      }

      return newText;
    }

    return td.innerText;
  }

  /**
   * Displays a warning next to a new item
   * @param {HTMLTableRowElement} itemRow The item row
   */
  _markUpNewItem(itemRow)
  {
    let itemName = itemRow.querySelector('.itemNameSpan');

    itemName.innerText = ('\u26A0 ' + itemName.innerText);

    itemName.title = 'This is a new item';
  }
  
  /**
   * 
   */
  constructor()
  {
    chrome.storage.local.get(['insider_decimal', 'insider_average', 'warn_new_items' ], (result) =>
    {
      for (let priceRange of document.querySelectorAll('.priceRange'))
      {
        priceRange.innerText = this._getNewCellText(priceRange, result.insider_decimal, result.insider_average);
      }

      if (result.warn_new_items)
      {
        for (let itemRow of document.querySelectorAll('.itemRow'))
        {
          if (itemRow.getAttribute('data-isnew') === '1')
          {
            this._markUpNewItem(itemRow);
          }
        }
      }
    });
  }
}

new Main();