class Main
{
  /**
   * @return {boolean} True if in new trade dialog
   */
  _isTradingNew()
  {
    if (document.URL === 'https://rocket-league.com/trading/new')
    {
      return true;
    }
    else if (document.URL.startsWith('https://rocket-league.com/trade/edit?trade='))
    {
      return true;
    }

    return false;
  }

  /**
   * @callback Main~getItemsCallback
   * @param {?Object} err An error object
   * @param {?Item[]} items The item list
   */
  /**
   * Requests an item list from the background extension.
   * @param {Main~getItemsCallback} callback
   */
  _getItems(callback)
  {
    chrome.runtime.sendMessage(null, 'getPrices', undefined, (response) =>
    {
      if (chrome.runtime.lastError)
      {
        callback(chrome.runtime.lastError);
      }
      else
      {
        if (response)
        {
          let itemResponse = {};

          for (let platform of Object.keys(response))
          {
            itemResponse[platform] = [];

            for (let rawItem of response[platform])
            {
              itemResponse[platform].push(Object.assign(new Item(), rawItem));
            }
          }

          callback(null, itemResponse);
        }
        else
        {
          callback({ msg: 'Empty response' });
        }
      }
    });
  }

  /**
   * Maps RLG platforms to local platforms
   * @param {string} rlgPlatform Platform id as used by RLG
   * @return {Platform} Local platform
   */
  _mapPlatform(rlgPlatform)
  {
    const PLATFORMS = {
      '1': Platform.PC,
      'rlg-user-networks-steam': Platform.PC,
      'Steam': Platform.PC,
      '2': Platform.PS,
      'rlg-user-networks-psn': Platform.PS,
      'PlayStation 4': Platform.PS,
      '3': Platform.XBOX,
      'rlg-user-networks-xbox': Platform.XBOX,
      'Xbox One': Platform.XBOX,
      // '4': Platform.SWITCH,
      // 'rlg-user-networks-switch': Platform.SWITCH,
      // 'Nintendo Switch': Platform.SWITCH,
    };

    const PLATFORM_FALLBACK = Platform.PC;

    if (Object.keys(PLATFORMS).includes(rlgPlatform))
    {
      return PLATFORMS[rlgPlatform];
    }
    else
    {
      console.warn(`Unknown platform ${rlgPlatform} falling back to ${Platform.name(PLATFORM_FALLBACK)}`);

      return PLATFORM_FALLBACK;
    }
  }

  /**
   * @param {HTMLDivElement} itemHolder The item's holder
   * @return {Platform} The item's platform
   */
  _getItemPlatform(itemHolder)
  {
    let displayContainer = itemHolder;

    do
    {
      displayContainer = displayContainer.parentElement;
    } while (!displayContainer.classList.contains('rlg-trade-display-container'));

    let platformRaw = displayContainer.querySelector('span').className;

    return this._mapPlatform(platformRaw);
  }

  /**
   * Extracts a single item from it's display container
   * @param {HTMLDivElement} display The display item
   * @return {Item} The item
   */
  _extractItem(display)
  {
    const NAMES = {
      'Player\’s Choice Crate 1': 'Player\'s Choice Crate',
    };

    const RARITIES = {
      'common': 'Common',
      'uncommon': 'Uncommon',
      'rare': 'Rare',
      'very-rare': 'Very Rare',
      'import': 'Import',
      'exotic': 'Exotic',
      'black-market': 'Black Market',
      'black market': 'Black Market',
      'limited': 'Limited',
      'premium': null,
    };

    const VEHICLES = {
      'Jäger 619 rs': 'Jäger 619 RS',
      'Octane zsr': 'Octane ZSR',
      'Road hog': 'Road Hog',
      'Breakout type-s': 'Breakout Type-S',
      'Animus gp': 'Animus GP',
      'X-devil': 'X-Devil',
      'Takumi rx-t': 'Takumi RX-T',
      'Dominus gt': 'Dominus GT',
      'Centio v17': 'Centio V17',
      'Imperator dt5': 'Imperator DT5',

      'Global': null,
    };

    const PAINTS = [
      null,
      'Burnt Sienna',
      'Lime',
      'Titanium White',
      'Cobalt',
      'Crimson',
      'Forest Green',
      'Grey',
      'Orange',
      'Pink',
      'Purple',
      'Saffron',
      'Sky Blue',
      'Black',
    ];

    if (display.getAttribute('draggable'))
    {
      // We're in /trading/new

      if (!display.getAttribute('data-item'))
      {
        // Empty display
        return undefined;
      }
    }

    let item = new Item();

    let h2 = display.querySelector('h2');

    let fullName = (h2 ? h2.innerHTML : display.getAttribute('data-name'));
    let itemName = fullName;

    let extraInfoIndex = fullName.indexOf('(');

    if (extraInfoIndex !== -1)
    {
      let extraInfo = fullName.substring(extraInfoIndex + 1, fullName.length - 1);

      if (extraInfo == 'Black Market')
      {
        item.setRarity('Black Market');
        itemName = fullName.substring(0, extraInfoIndex - 1);
      }
      else
      {
        if (['matte', 'Smooth'].includes(extraInfo))
        {
          itemName = fullName.replace('(matte)', '(Matte)');
        }
        else
        {
          extraInfo = (extraInfo[0].toUpperCase() + extraInfo.substr(1));

          if (extraInfo in VEHICLES)
          {
            item.setVehicle(VEHICLES[extraInfo]);
          }
          else
          {
            item.setVehicle(extraInfo);
          }

          itemName = fullName.substr(0, extraInfoIndex - 1);
        }
      }
    }

    if (itemName in NAMES)
    {
      item.setName(NAMES[itemName]);
    }
    else
    {
      item.setName(itemName);
    }

    let gradient = display.querySelector('.rlg-trade-item-gradient');

    let rawRarity = (gradient ? gradient.classList.item(1).substr(4) : display.getAttribute('data-rarity'));

    if (rawRarity in RARITIES)
    {
      item.setRarity(RARITIES[rawRarity]);
    }
    else
    {
      item.setRarity(rawRarity);
    }

    let paint = display.querySelector('.rlg-trade-display-item-paint');

    if (paint && paint.getAttribute('data-name'))
    {
      item.setPaint(paint.getAttribute('data-name'));
    }
    else
    {
      // /trading/new
      paint = PAINTS[JSON.parse(display.getAttribute('data-paint'))];

      if (paint)
      {
        item.setPaint(paint);
      }
    }

    let certification = display.querySelector('span');

    if (certification)
    {
      item.setCertification(certification.innerText);
    }
    else
    {
      certification = display.querySelector('.rlg-trade-display-item-cert');

      if (certification)
      {
        let text = certification.innerText;

        item.setCertification((text.length > 0) ? text.length : null);
      }
    }

    return item;
  }

  /**
   * Iterates all items, looking for a match for @item. If a match was found,
   * @item is populated with price provider information.
   * @param {Platform} platform Platform identifier
   * @param {Item} item The item to look for
   * @return {boolean} True if the item has been populated
   */
  _lookupItem(platform, item)
  {
    for (let providerItem of this._items[Platform.name(platform)])
    {
      if (providerItem.getName() !== item.getName())
      {
        continue;
      }

      // RLG has wrong rarities for vehicles
      // if (providerItem.getRarity() !== item.getRarity())
      // {
      //   continue;
      // }

      if (providerItem.getVehicle() !== item.getVehicle())
      {
        continue;
      }

      // Some people like painted keys
      if (providerItem.getName() !== 'Key')
      {
        if (providerItem.getPaint() !== item.getPaint())
        {
          continue;
        }
      }

      let certification = item.getCertification();
      Object.assign(item, providerItem);
      item.setCertification(certification);

      return true;
    }

    return false;
  }

  /**
   * Populates a single display item.
   * If there is no data available for this item, the stack property is removed.
   * @param {Platform} platform
   * @param {HTMLDivElement} displayItem The display item
   */
  _populateItem(platform, displayItem)
  {
    let item = this._extractItem(displayItem);

    if (item && this._lookupItem(platform, item))
    {
      let amount = displayItem.querySelector('.rlg-trade-display-item__amount');
      let stackSize = 1;

      if (amount)
      {
        stackSize = JSON.parse(amount.innerHTML);
      }
      else
      {
        // /trading/new
        stackSize = JSON.parse(displayItem.getAttribute('data-quantity'));
      }

      displayItem.stack = new ItemStack(item, stackSize || 1);
    }
    else
    {
      if (item)
      {
        if (item.getName() !== 'Offer')
        {
          console.log(`No match for ${item.getName()}`);
          console.log(item);
        }

        delete displayItem.stack;
      }
    }
  }

  /**
   * Populates items
   */
  _populateItems()
  {
    for (let itemHolder of document.querySelectorAll('.item-holder'))
    {
      let platform = this._getItemPlatform(itemHolder);

      let display = itemHolder.querySelector('.rlg-trade-display-item');

      this._populateItem(platform, display);
    }
  }

  /**
 * Adds a price tag to a single display item
 * @param {HTMLDivElement} displayItem The display item
 */
  _markUpPriceTag(displayItem)
  {
    let priceTag = document.createElement('a');
    priceTag.classList.add('priceTag');

    let singleDisplayPrice = (Math.round(displayItem.stack.getItem().getPrice() * 100.0) / 100.0).toString();

    let stackPrice = displayItem.stack.getPrice();

    if (displayItem.stack.getPrice() === 0.0)
    {
      priceTag.innerText = '-';
    }
    else if (displayItem.stack.getSize() > 1)
    {
      priceTag.innerText = `${displayItem.stack.getSize()}\u22C5${singleDisplayPrice} = ${(Math.round(stackPrice * 100.0) / 100.0).toString()}`;
    }
    else
    {
      priceTag.innerText = singleDisplayPrice;
    }

    let actionBar = (displayItem.parentElement.querySelector('.action-bar') ||
      displayItem.parentElement.parentElement.querySelector('.action-bar'));

    if (actionBar.firstChild)
    {
      actionBar.insertBefore(priceTag, actionBar.firstChild);
    }
    else
    {
      actionBar.appendChild(priceTag);
    }
  }


  /**
   * Highlights profitable trades
   */
  _markUpHotTrades()
  {
    for (let trade of document.querySelectorAll('.rlg-trade-display-items'))
    {
      let yourItems = trade.querySelector('#rlg-youritems'); // Left
      let theirItems = trade.querySelector('#rlg-theiritems'); // Right

      if (yourItems.children.length === theirItems.children.length)
      {
        for (let i = 0; i < yourItems.children.length; ++i)
        {
          let yourItem = yourItems.children[i].querySelector('.rlg-trade-display-item');
          let theirItem = theirItems.children[i].querySelector('.rlg-trade-display-item');

          if (yourItem && theirItem)
          {
            if (yourItem.stack && theirItem.stack)
            {
              let yourPrice = yourItem.stack.getPrice();
              let theirPrice = theirItem.stack.getPrice();

              if (yourPrice && theirPrice)
              {
                if (yourPrice > theirPrice)
                {
                  yourItem.classList.add('hotTrade');
                  theirItem.classList.add('hotTrade');
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * Adds an insider.gg URL to the given item.
   * @param {HTMLDivElement} displayItem The display item
   */
  _markUpURL(displayItem)
  {
    let item = displayItem.stack.getItem();
    let insiderSource = item.getSource('insider.gg');

    if (insiderSource)
    {
      let a = document.createElement('a');
      a.href = insiderSource;

      let img = document.createElement('img');
      img.src = 'https://rl.insider.gg/img/icons/logoSmall.png';
      img.style.width = '1em';
      img.style.height = '1em';

      a.appendChild(img);

      let actionBar = (displayItem.parentElement.querySelector('.action-bar') ||
        displayItem.parentElement.parentElement.querySelector('.action-bar'));

      actionBar.appendChild(a);
    }
  }

  /**
   * Adds a "new item" warning to the given item.
   * @param {HTMLDivElement} itemHolder The item's holder
   */
  _markUpWarnNewItem(itemHolder)
  {
    let warning = document.createElement('span');

    warning.classList.add('new-item-warning');
    warning.title = 'This is a new item';

    let actionBar = itemHolder.querySelector('.action-bar');
    actionBar.insertBefore(warning, actionBar.firstChild);
  }

  /**
   * @callback Main~_markUpDynamicItemHolderCallback
   */
  /**
   * Installs an update listener to dynamically update item holders.
   * This is used for /trading/new, where items can be added and removed.
   * @param {HTMLDivElement} itemHolder The item holder
   * @param {Main~_markUpDynamicItemHolderCallback} callback Gets called once
   *                                                         all mark up has
   *                                                         been applied.
   */
  _markUpDynamicItemHolder(itemHolder, callback)
  {
    let platform = this._mapPlatform(document.querySelector('#platform_chosen span').innerText);

    // Clear the existing information
    let actionBar = itemHolder.querySelector('.action-bar');

    while (actionBar.hasChildNodes())
    {
      actionBar.removeChild(actionBar.firstChild);
    }

    let displayItem = itemHolder.querySelector('.rlg-trade-display-item');

    if (displayItem.getAttribute('data-item'))
    {
      this._populateItem(platform, displayItem);

      chrome.storage.local.get(['rlg_price'], (result) =>
      {
        this._markUpItemHolder(itemHolder, result);
        callback && callback();
      });
    }
    else
    {
      // The item holder's item was removed
      callback && callback();
    }
  }

  /**
   * Installs a dynamic item holder's update callback
   */
  _installDynamicItemHolderUpdateCallback(itemHolder)
  {
    let observe = (obs) =>
    {
      obs.observe(itemHolder, {
        childList: true,
        attributes: true,
        subtree: true,
      });
    };

    itemHolder.obs = new MutationObserver((mutationsList) =>
    {
      // Disconnect {@link obs}, because {@link _markUpDynamicItemHolder} would
      // cause an infinite loop of updates.
      itemHolder.obs.disconnect();

      this._markUpDynamicItemHolder(itemHolder, () =>
      {
        observe(itemHolder.obs);
      });
    });

    observe(itemHolder.obs);
  }

  /**
   * Install a platform change listener on /trading/new
   */
  _installPlatformChangeListener()
  {
    let interval = setInterval(() =>
    {
      let platformChosen = document.querySelector('#platform_chosen span');

      if (platformChosen)
      {
        clearInterval(interval);
        let obs = new MutationObserver((mutationsList) =>
        {
          for (let itemHolder of document.querySelectorAll('.item-holder'))
          {
            itemHolder.obs.disconnect();

            this._markUpDynamicItemHolder(itemHolder, () =>
            {
              itemHolder.obs.observe(itemHolder, {
                childList: true,
                attributes: true,
                subtree: true,
              });
            });
          }
        });

        obs.observe(platformChosen, {
          attributes: true,
          childList: true,
          subtree: true,
        });
      }
    }, 100);
  }

  /**
   * Adds item lookup controls
   */
  _markUpLayoutItemLookup()
  {
    const LOOKUP_MAX_PAGES = 5;

    let lookupPageCount = Math.min(LOOKUP_MAX_PAGES, document.querySelector('.rlg-trade-pagination').children.length - 2);

    if (lookupPageCount === 0)
    {
      return;
    }

    let template = document.createElement('template');

    template.innerHTML = `
      <div id='item-lookup'>
        <button id='lookup-search' class='rlg-btn-secondary'>LOOK UP PRICES OF ${lookupPageCount} PAGE(S)</button>
        <div id='lookup-results' hidden>
          <progress min=0 max=100 value=0></progress><br>
          <span id='error' hidden></span>
          <div id='no-error'>
            <span>Average price: </span><span id='lookup-average'>-</span><br>
            <span>Trades</span><br>
            <div id='lookup-trades'>
            </div>
          </div>
        </div>
      </div>
    `;

    let filterContent = document.querySelector('.rlg-trade-filter-content');

    filterContent.appendChild(template.content);

    let itemLookup = filterContent.querySelector('#item-lookup');

    let btnSearch = itemLookup.querySelector('#lookup-search');
    let progressBar = itemLookup.querySelector('progress');
    let error = itemLookup.querySelector('#error');
    let noError = itemLookup.querySelector('#no-error');
    let averagePrice = itemLookup.querySelector('#lookup-average');
    let lookupResults = itemLookup.querySelector('#lookup-results');
    let tradeResults = itemLookup.querySelector('#lookup-trades');

    progressBar.style.width = `${btnSearch.clientWidth}px`;

    let setErrorMessage = (msg) =>
    {
      error.innerText = msg;

      error.hidden = false;
      noError.hidden = true;
    };

    btnSearch.onclick = () =>
    {
      btnSearch.disabled = true;
      lookupResults.hidden = false;

      let lookup = new RLGPriceLookup();

      if (lookup.setItemFromURL(document.URL))
      {
        let loadedOfferCount = 0;

        lookup.run(lookupPageCount, false, (err, progress, offers) =>
        {
          if (err)
          {
            setErrorMessage('Could not load page(s), see console for details.');
            console.log(err);
          }
          else
          {
            if (progress === 1.0)
            {
              progressBar.hidden = true;
            }

            if ((progress === 1.0) && (offers.length === 0))
            {
              setErrorMessage('No suitable trades found.');
            }
            else
            {
              progressBar.value = (progress * 100);

              for (let i = loadedOfferCount; i < offers.length; ++i)
              {
                let offer = offers[i];
                let insertBefore = null;

                for (let j = 0; j < tradeResults.children.length; ++j)
                {
                  if (offer.offer < tradeResults.children[j].offer.offer)
                  {
                    insertBefore = tradeResults.children[j];

                    break;
                  }
                }

                let a = document.createElement('a');

                a.href = offer.trade;
                a.innerText = ` ${(Math.round(offer.offer * 100.0) / 100.0).toString()} `;
                a.offer = offer;

                tradeResults.insertBefore(a, insertBefore);
              }

              loadedOfferCount = offers.length;

              let average = 0.0;

              for (let tradeResult of tradeResults.children)
              {
                average += tradeResult.offer.offer;
              }

              average /= tradeResults.children.length;

              averagePrice.innerText = (Math.round(average * 100.0) / 100.0).toString();
            }
          }
        });
      }
      else
      {
        setErrorMessage('Please select a search type.');
      }
    };
  }

  /**
   * Converts the original layout to one that allows easier customization.
   */
  _markUpLayout()
  {
    for (let display of document.querySelectorAll('.rlg-trade-display-item'))
    {
      let a = display.parentElement;

      if (this._isTradingNew())
      {
        // /trading/new doesn't have <a>'s
        a = display;
      }

      let itemContainer = a.parentElement;

      let div = document.createElement('div');
      div.classList.add('item-holder');

      itemContainer.replaceChild(div, a);

      div.appendChild(a);

      let actionBar = document.createElement('div');
      actionBar.classList.add('action-bar');
      div.appendChild(actionBar);
    }

    if (document.URL.startsWith('https://rocket-league.com/trading?'))
    {
      this._markUpLayoutItemLookup();
    }
  }

  /**
   * Adds information to a single item holder's item
   * @param {HTMLDivElement} itemHolder The item holder
   * @param {Object} options The options, as stored in local storage
   */
  _markUpItemHolder(itemHolder, options)
  {
    let displayItem = itemHolder.querySelector('.rlg-trade-display-item');

    if (displayItem.stack)
    {
      if (options.rlg_price)
      {
        this._markUpPriceTag(displayItem);
      }

      if (options.warn_new_items && displayItem.stack.getItem().isNew())
      {
        this._markUpWarnNewItem(itemHolder);
      }

      this._markUpURL(displayItem);
    }
  }


  /**
   * Adds custom information to all items.
   * Assumes a custom layout and populated items.
   */
  _markUp()
  {
    chrome.storage.local.get(['rlg_price', 'rlg_hottrade', 'warn_new_items'], (result) =>
    {
      for (let itemHolder of document.querySelectorAll('.item-holder'))
      {
        this._markUpItemHolder(itemHolder, result);
      }

      if (result.rlg_hottrade)
      {
        this._markUpHotTrades();
      }

      if (this._isTradingNew())
      {
        for (let itemHolder of document.querySelectorAll('.item-holder'))
        {
          this._installDynamicItemHolderUpdateCallback(itemHolder);
        }
      }
    });
  }

  /**
   * 
   */
  constructor()
  {
    // Trades are sent via html, we don't need to wait for them to load.

    this._markUpLayout();

    this._getItems((err, items) =>
    {
      if (err)
      {
        console.error(err);
      }
      else
      {
        this._items = items;

        this._populateItems();

        if (this._isTradingNew())
        {
          this._installPlatformChangeListener();
        }

        this._markUp();
      }
    });
  }
}

new Main();