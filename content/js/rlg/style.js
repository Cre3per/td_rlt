class BackgroundLoader
{
  /**
   * Fits the canvas to the window's size and sets the image.
   */
  _fitCanvas()
  {
    this._canvas.width = window.innerWidth;
    this._canvas.height = window.innerHeight;

    let d2 = this._canvas.getContext('2d');

    d2.filter = 'blur(50px)';

    d2.drawImage(this._img, 0, 0);
  }

  /**
   * Fired when document.head exists
   */
  _onHeadReady()
  {
    let link = document.createElement('link');

    link.href = chrome.runtime.getURL('/content/css/rlgdark.css');
    link.type = 'text/css';
    link.rel = 'stylesheet';

    document.head.appendChild(link);
  }

  /**
   * Fired when document.body exists
   */
  _onBodyReady()
  {
    this._img.onload = (el, ev) =>
    {
      document.body.appendChild(this._canvas);

      this._fitCanvas();

      window.onresize = (el, ev) =>
      {
        this._fitCanvas();
      };
    };

    this._img.src = chrome.runtime.getURL('/content/assets/background.png');
  }

  /**
   * 
   */
  _nodeListHasNode(nodeList, node)
  {
    for (let n of nodeList)
    {
      if (n === node)
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Activates the dark theme
   */
  _loadDarkTheme()
  {
    this._img = document.createElement('img');
    this._canvas = document.createElement('canvas');

    this._canvas.id = 'background-image';

    let hasHead = false;
    let hasBody = false;

    if (document.head)
    {
      this._onHeadReady();
      hasHead = true;
    }

    if (document.body)
    {
      this._onBodyReady();
      hasBody = true;
    }

    // This file is loaded at document_start, {@link document} doesn't have any
    // children just yet.
    let obs = new MutationObserver((mutationsList) =>
    {
      if (document.head && document.body)
      {
        obs.disconnect();
      }

      if (!hasHead && document.head)
      {
        this._onHeadReady();
        hasHead = true;
      }
  
      if (!hasBody && document.body)
      {
        this._onBodyReady();
        hasBody = true;
      }
    });

    obs.observe(document,
      {
        attributes: false,
        childList: true,
        subtree: true
      });
  }

  /**
   * 
   */
  constructor()
  {
    chrome.storage.local.get([ 'rlg_dark' ], (result) =>
    {
      if (result.rlg_dark)
      {
        this._loadDarkTheme();
      }
    });
  }
}

new BackgroundLoader();