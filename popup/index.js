class ExtensionOption
{
  /**
   * @return {HTMLElement}
   */
  createControl()
  {
    if ((typeof this.defaultValue) === 'boolean')
    {
      let control = document.createElement('div');
      let input = document.createElement('input');
      let label = document.createElement('label');

      control.id = this.name;
      control.title = this.description;

      input.id = `input_${this.name}`;
      input.type = 'checkbox';
      input.select = this._defaultValue;

      label.htmlFor = `input_${this.name}`;
      label.innerText = this.displayName;

      control.appendChild(input);
      control.appendChild(label);

      input.checked = this.defaultValue;

      chrome.storage.local.get([this.name], (result) =>
      {
        if (this.name in result)
        {
          input.checked = result[this.name];
        }
        else
        {
          let newOption = { };
          newOption[this.name] = this.defaultValue;
          chrome.storage.local.set(newOption);
        }
      });

      input.onchange = () =>
      {
        let newOption = {};
        newOption[this.name] = input.checked;

        chrome.storage.local.set(newOption);

        console.log(newOption);
      };

      return control;
    }

    console.warn('Unknown option type ' + (typeof this.defaultValue));

    return undefined;
  }

  /**
   * @param {string} name
   * @param {Object} defaultValue
   * @param {string} displayName
   * @param {string} description
   */
  constructor(name, defaultValue, displayName, description = null)
  {
    /**
     * Name
     * @type {string}
     */
    this.name = name;

    /**
     * Default value
     * @type {Object}
     */
    this.defaultValue = defaultValue;

    /**
     * Display name
     * @type {string}
     */
    this.displayName = displayName;

    /**
     * Description
     * @type {string}
     */
    this.description = description;
  }
}

class Main
{
  /**
   * Registers options
   */
  _registerOptions()
  {
    this._options.push(new ExtensionOption('rlg_dark', true, 'Dark garage', 'Use a dark theme on rocket-league.com'));
    this._options.push(new ExtensionOption('rlg_hottrade', true, 'Highlight hot trades', 'Highlight profitable trades on rocket-league.com'));
    this._options.push(new ExtensionOption('rlg_price', true, 'Show prices', 'Show prices on rocket-league.com'));

    this._options.push(new ExtensionOption('insider_decimal', true, 'Use decimal prices', 'Show decimal prices on rl.insider.gg'));
    this._options.push(new ExtensionOption('insider_average', true, 'Show average', 'Show average prices on rl.insider.gg'));

    this._options.push(new ExtensionOption('warn_new_items', true, 'Warn about new items', 'Display a warning for items that are new'));
  }

  /**
   * Creates controls for all options
   */
  _displayOptions()
  {
    for (let option of this._options)
    {
      console.log(option);

      let control = option.createControl();

      document.querySelector('#settings').appendChild(control);
    }
  }

  /**
   * 
   */
  constructor()
  {
    /**
     * All options
     * @type {ExtensionOption[]}
     */
    this._options = [];

    this._registerOptions();

    window.onload = () => 
    {
      this._displayOptions();
    };
  }
}

new Main();